locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "virtualbox-iso" "autogenerated" {
  boot_command         = [
                           "<enter>",
                           "<wait90>",
                           "passwd root<enter><wait5>root-password<enter><wait5>root-password<enter><wait5>",
                           "<enter><wait5>",
                           "sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config",
                           "<enter><wait5>",
                           "echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config",
                           "<enter><wait5>",
                           "/etc/init.d/sshd start",
                           "<enter><wait5>"
                         ]
  boot_wait            = "5s"
  disk_size            = 16384
  guest_additions_mode = "disable"
  guest_os_type        = "Gentoo_64"
  iso_checksum         = "sha256:bf21a8905e868598b81c4673e0bf7827401b26426b30d0eb53c3ea4d31ce4c25"
  iso_url              = "https://mirror.yandex.ru/gentoo-distfiles/releases/amd64/autobuilds/current-install-amd64-minimal/install-amd64-minimal-20230122T154655Z.iso"
  shutdown_command     = "shutdown -hP now"
  ssh_agent_auth       = false
  ssh_password         = "root-password"
  ssh_port             = 22
  ssh_timeout          = "10000s"
  ssh_username         = "root"
  vboxmanage           = [
                           ["modifyvm", "{{.Name}}", "--memory", "4096"],
                           ["modifyvm", "{{.Name}}", "--cpus", "2"],
                         ]
  vm_name              = "gentoo-from-packer"
}

build {
  sources = ["source.virtualbox-iso.autogenerated"]

  provisioner "file" {
    destination = "/tmp"
    source      = "scripts"
  }
  provisioner "shell" {
    environment_vars = [
                         "GENTOO_MIRROR=https://mirror.yandex.ru/gentoo-distfiles/",
                         "ROOT_PASSWORD=Very4dumb%password",
                         "VM_TYPE=virtualbox"
                       ]
    scripts          = ["init.sh"]
  }
}
