#!/bin/bash

set -xe

chmod +x /tmp/scripts/*.sh

/tmp/scripts/01_bootstrap.sh
/tmp/scripts/02_environment.sh

echo "All done."
