#!/bin/sh

GENTOO_ARCH=amd64
GENTOO_STAGE3=amd64-systemd
ROOT_PASSWORD=${ROOT_PASSWORD:-}
TARGET_DISK=/dev/sda

set -xe

echo "### Checking configuration..."
if [ -z "$ROOT_PASSWORD" ]; then
    echo "ROOT_PASSWORD must be set to continue"
    exit 1
fi

echo "### Setting time..."
ntpd -gq

echo "### Creating partitions..."
sgdisk \
  -n 1:0:+128M -t 1:8300 -c 1:"boot" \
  -n 2:0:+32M  -t 2:ef02 -c 2:"bios"  \
  -n 3:0:+1G   -t 3:8200 -c 3:"swap"  \
  -n 4:0:0     -t 4:8300 -c 4:"root"  \
  -p ${TARGET_DISK}
sync

mkfs.ext4 -L boot /dev/sda1
mkfs.ext4 -L root /dev/sda4

echo "### Creating SWAP..."
mkswap -L swap ${TARGET_DISK}3
swapon ${TARGET_DISK}3

echo "### Mounting partitions..."
mkdir -p /mnt/gentoo
mount ${TARGET_DISK}4 /mnt/gentoo

mkdir -p /mnt/gentoo/boot
mount ${TARGET_DISK}1 /mnt/gentoo/boot

echo "### Setting work directory..."
cd /mnt/gentoo

echo "### Installing stage3..."
STAGE3_PATH_URL="$GENTOO_MIRROR/releases/$GENTOO_ARCH/autobuilds/latest-stage3-$GENTOO_STAGE3.txt"
STAGE3_PATH=$(curl -s "$STAGE3_PATH_URL" | grep -v "^#" | cut -d" " -f1)
STAGE3_URL="$GENTOO_MIRROR/releases/$GENTOO_ARCH/autobuilds/$STAGE3_PATH"

wget "$STAGE3_URL"

tar xvpf "$(basename "$STAGE3_URL")" --xattrs-include='*.*' --numeric-owner

rm -fv "$(basename "$STAGE3_URL")"

echo "### Installing LiveCD kernel..."
LIVECD_KERNEL_VERSION=$(cut -d " " -f 3 < /proc/version)

cp -v "/mnt/cdrom/boot/gentoo" "/mnt/gentoo/boot/vmlinuz-$LIVECD_KERNEL_VERSION"
cp -v "/mnt/cdrom/boot/gentoo.igz" "/mnt/gentoo/boot/initramfs-$LIVECD_KERNEL_VERSION.img"
cp -vR "/lib/modules/$LIVECD_KERNEL_VERSION" "/mnt/gentoo/lib/modules/"

echo "### Installing kernel configuration..."
mkdir -p /mnt/gentoo/etc/kernels
cp -v /etc/kernels/* /mnt/gentoo/etc/kernels

echo "### Copying network options..."
cp -v /etc/resolv.conf /mnt/gentoo/etc/

echo "### Configuring fstab..."
cat >> /mnt/gentoo/etc/fstab << END

# added by gentoo installer
LABEL=boot /boot ext4 noauto,noatime 1 2
LABEL=swap none  swap sw             0 0
LABEL=root /     ext4 noatime        0 1
END

echo "### Mounting proc/sys/dev..."
mount -t proc none /mnt/gentoo/proc
mount -t sysfs none /mnt/gentoo/sys
mount -o bind /dev /mnt/gentoo/dev
mount -o bind /dev/pts /mnt/gentoo/dev/pts
mount -o bind /dev/shm /mnt/gentoo/dev/shm

echo "### Changing root..."
chroot /mnt/gentoo /bin/bash -s << END
#!/bin/bash

set -xe

echo "### Upading configuration..."
env-update
source /etc/profile

echo "### Installing portage..."
mkdir -p /etc/portage/repos.conf
cp -f /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
emerge-webrsync

echo "### Installing bootloader..."
emerge grub

cat >> /etc/portage/make.conf << IEND

# added by gentoo installer
GRUB_PLATFORMS="pc"
IEND

cat >> /etc/default/grub << IEND

# added by gentoo installer
GRUB_DEFAULT=0
GRUB_TIMEOUT=0
IEND

grub-install ${TARGET_DISK}
grub-mkconfig -o /boot/grub/grub.cfg

echo "### Configuring network..."
cat >> /etc/systemd/network/20-wired.network << IEND
[Match]
Name=en*

[Network]
DHCP=yes
IEND

systemd-machine-id-setup
systemctl enable systemd-resolved.service
systemctl enable systemd-networkd.service

echo "### Configuring root password..."
echo "root:$ROOT_PASSWORD" | chpasswd
END

echo "### Gentoo installation completed..."
