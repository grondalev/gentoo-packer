#!/bin/sh

set -xe

echo "### Changing root..."

chroot /mnt/gentoo /bin/bash -s << END
#!/bin/bash

set -xe

echo "### Upading configuration..."

env-update
source /etc/profile

echo "### Installing additional software..."

emerge app-containers/docker app-containers/docker-cli
emerge sys-cluster/kubectl

curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.17.0/kind-linux-amd64
chmod +x ./kind
mv ./kind /usr/local/bin/kind

END

echo "### Additional software installation completed..."
